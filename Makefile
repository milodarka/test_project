APP_CONTAINER=docker exec -it FactorBlue_php /bin/sh
PG_CONTAINER= FactorBlue_postgres

startup:
	docker-compose up -d

start:
	docker-compose up -d
	
	$(APP_CONTAINER) "composer install --no-interaction; chown -R www-data:www-data . \
	php artisan cache:clear; php artisan route:clear; php artisan migrate; php artisan db:seed"
	sudo docker restart $(PG_CONTAINER)
kill:
	docker-compose -f docker-compose.yml down
	docker image prune --all --force
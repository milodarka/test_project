<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>{{$title}}</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/checkout/">
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">

    <div class="container">
        <main>
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="../../img/logo.png" alt="" width="100" height="100"
                    height="57">
                <h2>Logged user info</h2>
                <p class="lead"></p>
            </div>

            <form action="{{route('user-edit')}}" method='post'>
                @csrf
                <input type='hidden' value="{{ $user->id }}" name='user_id'>
                <div class="row g-5">
                    <div class="col-sm-3">
                        <label for="firstName" class="form-label">Name</label>
                        <input type='text' id="lastName" class="form-control" value="{{ $user->name }}" name='name'>
                    </div>
                    <div class="col-sm-3">
                        <label for="lastName" class="form-label">Last name</label>
                        <input type='text' class="form-control" id="lastName" value="{{ $user->lastname }}"
                            name='lastname'>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="email" class="form-label">Email</label>
                        <input type='text' class="form-control" id="email" value="{{ $user->email }}" name='email'>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <button class="w-100 btn btn-primary btn-lg mt-3" type="submit">Update</button>
                    </div>
                </div>
            </form>


            @foreach($address as $addr=>$val)
            <form action="{{route('address-edit')}}" method='post'>
                @csrf
                <div class="row g-3 mt-3">

                    <div class="col-sm-3">
                        <label for="firstName" class="form-label">Address type</label>
                        <select class="form-select" id="inputGroupSelect01" name="user_address_type_id">
                            @foreach($types as $address_type)
                            <option value="{{ $address_type->id  }}"@if($val->user_address_type_id == $address_type->id) selected @endif>
                                {{ $address_type->type }}</option>

                            @endforeach
                        </select>

                    </div>

                    <div class="col-sm-3">
                        <label for="address" class="form-label">Address</label>
                        <input type='text' id="address" class="form-control" value="{{ $val->address }}" name='adress'>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <label for="city" class="form-label">City</label>
                        <input type='text' class="form-control" id="city" value="{{ $val->city }}" name='city'>
                        <div class="invalid-feedback">
                            Valid city is required.
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <label for="state" class="form-label">State</label>
                        <input type='text' class="form-control" id="state" value="{{ $val->state }}" name='state'>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                    <input type='hidden' value="{{ $val->user_address_id }}" name='id'>
                    <input type='hidden' value="{{ $val->user_id }}" name='user_id'>

                    <div class="col-sm-2">
                        <button class="btn btn-primary mt-3" type="submit">Update</button>
                    </div>
                </div>
            </form>
            @endforeach
            <div class="offset-sm-5">
                <button class="btn btn-primary mt-3" ><a href= "{{route('logout')}}">Logout </a></button>
            </div>

        </main>
    </div>
</body>

</html>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>{{$title}}</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/checkout/">
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <link href="form-validation.css" rel="stylesheet">
  </head>

<body class="bg-light">

    <div class="container float-md-end">
      <main>
        <div class="py-5 text-center col-8">
          <img class="d-block mx-auto mb-4"src="./img/logo.png" alt="" width="200" height="150">
          <h2>Register</h2>
        </div>

        <div class="row g-5">
          <div class="col-md-7 col-lg-8">
            <h4 class="mb-3">User data</h4>
            <form action="{{route('user-registration')}}" method='post'>
              @csrf
              @if(Session::has('fail'))
              <div>{{Session::get('fail')}}</div>
              @endif
              <input type="hidden" value="1" name="active">
              <div class="row g-3">
                <div class="col-sm-6">
                  <label for="firstName" class="form-label">First name</label>
                  <input type="text" class="form-control" id="firstName" name="name" placeholder="name" value="{{old("name")}}" required>
                  <span class="text-danger">@error('name') {{$message}} @enderror </span>
                </div>

                <div class="col-sm-6">
                  <label for="lastName" class="form-label">Last name</label>
                  <input type="text" class="form-control" id="lastName" placeholder="" value="{{ old('lastname') }}" required name="lastname">
                  <span class="text-danger">@error('lastname') {{$message}} @enderror </span>
                </div>

                <div class="col-12">
                  <label for="email" class="form-label">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="you@example.com" name="email">
                  <span class="text-danger">@error('email') {{$message}} @enderror </span>
                </div>

                <div class="col-12">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Enter your password here" name="password">
                  <span class="text-danger">@error('password') {{$message}} @enderror </span>
                </div>
              </div>
              <div>
                <table id="multiple_address">
                    <thead>
                        <tr>
                            <input type="hidden" value="" name="user_address_id">
                            <td><label for='address'>Address type</label>
                              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Add more address types
                              </button>
                            </td>

                            <td> <label for='address'>Address</label></td>
                            <td> <label for='city'>City</label></td>
                            <td> <label for='state'>State</label></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="col0">
                                <select class="form-control" name ='user_address_type_id'>
                                    @foreach($types as $address_type)
                                    <option value="{{ $address_type->id }}">{{$address_type->type }}</option>

                                    @endforeach
                                </select>
                            </td>
                            <td id="col1"> <input type="address" name="address[]" class="form-control" placeholder="address" required></td>
                            <td id="col2">  <input type="text" name="city[]" class="form-control"  placeholder="city" required></td>
                            <td id="col3"> <input type="text" name="state[]" class="form-control" placeholder="state" required></td>
                            <td><button type="button" class="btn btn-sm btn-info" onclick="addRows()">Add</button></td>
                            <td><button type="button" class="btn btn-sm btn-danger" onclick="deleteRows()">Remove</button></td>
                        </tr>
                    </tbody>
                    </table>
                    <br>
                    <br>
                </div>
             <hr class="my-4">
              <button class="w-100 btn btn-primary btn-lg" type="submit">Register</button>
            </form>

            <button class="w-100 btn btn-lg"> If you already have an account, you can  <a href="login">Login</a></button>
          </div>
        </div>
      </main>
    </div>
<!-- Modal -->
    <div class="modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Add more address types</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
            <div class="modal-body">
              <form action="{{ route('add-address-type')}}" method="POST" autocomplete="off">
                @csrf
                    <input type="text" name="type">
                    <input type="hidden" name="user_address_type_id">
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <input type="submit" value="save" name="submit" class="btn btn-primary">
          </form>
          </div>
        </div>
      </div>
<!-- Modal End-->

    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="form-validation.js"></script>
    <script>
      function addRows()
      {
          var table = document.getElementById('multiple_address');
          var rowCount = table.rows.length;
          var cellCount = table.rows[0].cells.length;
          var row = table.insertRow(rowCount);
          for(var i =0; i <= cellCount; i++)
          {
              var cell = 'cell'+i;
              cell = row.insertCell(i);
              var copycel = document.getElementById('col'+i).innerHTML;
              cell.innerHTML=copycel;

          }
      }

      function deleteRows()
      {
          var table = document.getElementById('multiple_address');
          var rowCount = table.rows.length;
          if(rowCount > '2')
          {
              var row = table.deleteRow(rowCount-1);
              rowCount--;
          }else{
              alert('There should be atleast one row');
          }
      }
      var myModal = document.getElementById('myModal')
      var myInput = document.getElementById('myInput')

      myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus()
      })
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' type='text/javascript'></script>
  </body>
</html>
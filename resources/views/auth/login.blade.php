<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>{{$title}}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
  </head>
  <body class="text-center">

<main class="form-signin">
  <form action="{{ route('login-check')}}"method="POST" autocomplete="off">
    @if(Session::get('fail'))
                    <div class="alert alert-danger">
                        {{Session::get('fail');}}
                    </div>
                @endif
                @csrf
    <img class="mb-4" src="./img/logo.png" alt="" width="200" height="150">
    <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

    <div class="form-floating">
      <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" value = "{{ old('email') }}" name="email">
      <label for="floatingInput">Email address</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
      <label for="floatingPassword">Password</label>
      <span class="text-danger">@error('email'){{ $message }} @enderror </span>
    </div>

    <div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label>
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
  </form>
  <div>

  <button class="w-100 btn btn-lg"> If you don't have an account, you can  <a href="register">register</a></button>
  </div>
</main>



  </body>
</html>

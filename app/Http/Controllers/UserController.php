<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index($user_id)
    {
        $user = User::getById($user_id);
        $address = UserAddress::getAddressesByUserID($user_id);
        $data = array(
            "user" => $user,
            "types" => UserAddress::getAllAddressTypes(),
            "title" => 'user info',
            "address" => $address
        );

        return view('user.main')->with($data);
    }

    public function registration(Request $request)
    {

        $request->validate([
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4|max:16',

        ]);

        $user_info = [
            'name' => $request->name,
            'lastname' => $request->lastname,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'active' => $request->active
        ];

        if ($user_info) {
            DB::table('users')->insert($user_info);
        } else {
            return back()->with('Something went wrong');
        }
        $query = false;

        for ($i = 0; $i < count($request->address); $i++) {
            $adress_info = [
                'adress' => $request->address[$i],
                'city' => $request->city[$i],
                'state' => $request->state[$i],
                'user_id' => User::getLastRecordedId(),
                'user_address_type_id' =>  $request->user_address_type_id

            ];
            DB::table('user_address')->insert($adress_info);
            $query = true;
        }
        if ($user_info && $query == true) {
            return redirect('user/user_home/' . User::getLastRecordedId());
        } else {
            return back()->with('Something went wrong');
        }
    }


    public function address_type(Request $request)
    {
        $request->validate([
            'type' => 'required|min:2|max:14'
        ]);
        $address_type = [
            'type' => $request->type
        ];
        if ($address_type) {
            DB::table('user_address_type')->insert($address_type);
            return back()->with('Saved succesfuly');
        } else {
            return back()->with('Something went wrong');
        }
    }
    public function edit_user(Request $request)
    {
        $request->validate([
            'name' => 'required|min:4',
            'lastname' => 'required|min:4',
            'email' => 'required|email|unique:users'

        ]);

        $user_info = [
            'name' => $request->name,
            'email' => $request->email,
            'lastname' => $request->lastname
        ];

        if ($user_info) {
            DB::table('users')->where('id', $request->user_id)->update($user_info);
            return redirect('user/user_home/' . $request->user_id);
        } else {
            return back()->with('Something went wrong');
        }
    }
    public function address_edit(Request $request)
    {
        $request->validate([
            'adress' => 'required|min:4',
            'city' => 'required|min:4',
            'state' => 'required|min:4'
        ]);
        $adress_info = [
            'adress' => $request->adress,
            'city' => $request->city,
            'state' => $request->state,
            'user_address_type_id' =>  $request->user_address_type_id

        ];
        if ($adress_info) {
            DB::table('user_address')->where('id', $request->id)->update($adress_info);
            return redirect('user/user_home/' . $request->user_id);
        } else {
            return back()->with('Something went wrong');
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function index()
    {
        $data = array(
            "title" => '**welcome**',
        );
        return view('hello')->with($data);
    }
    public function login()
    {
        $data = array(
            "title" => 'login',
        );
        return view('auth.login')->with($data);
    }
    public function login_check(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:4|max:16'
        ]);
        $user = User::where('email', '=', $request->email)
            ->first();
        if (!$user) {
            return back()->with('fail', 'Wrong email');
        } else {
            if (Hash::check($request->password, $user->password)) {
                $request->session()->put('UserLoggged', $user->id);
                return redirect('user/user_home/' . $user->id);
            } else {
                return back()->with('fail', 'Wrong password');
            }
        }
    }
    public function register()
    {
        $data = array(
            "title" => 'registration',
            "types" => UserAddress::getAllAddressTypes()
        );
        return view('auth.register')->with($data);
    }
}
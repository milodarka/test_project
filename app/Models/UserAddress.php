<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserAddress extends Model
{
    use HasFactory;

    protected $table_name = 'user_address';

    protected $fillable = [
        'address',
        'city',
        'state'
    ];
    public static function getAddressesByUserID(int $user_id)
    {

        $addresses = DB::table('user_address')
            ->select('user_address_type.type as type', 'user_address.id as user_address_id', 'user_address.adress as address', 'user_address.city as city', 'user_address.state as state', 'user_address_type.id as user_address_type_id', 'user_address.user_id as user_id')
            ->distinct()
            ->join('user_address_type', 'user_address_type.id', '=', 'user_address.user_address_type_id')
            ->where('user_address.user_id',  $user_id)->get();

        return $addresses;
    }
    public static function getAddressType(int $address_type_id)
    {
        $address_type = DB::table('user_address_type')->where('user_address_type_id', $address_type_id)->first();
        return $address_type->type;
    }
    public static function getAllAddressTypes()
    {
        $types = DB::table('user_address_type')->orderBy('id', 'asc')->get();
        return $types;
    }
    
}